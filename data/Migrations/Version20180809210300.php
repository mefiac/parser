<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180809210300 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('news');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('full_text', 'text', ['notnull' => true, 'length' => 9000]);
        $table->addColumn('header_text', 'text', ['notnull' => true, 'length' => 500]);
        $table->addColumn('link', 'string', ['notnull' => true, 'length' => 256]);
        $table->addColumn('image', 'text', ['notnull' => false, 'length' => 900]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine', 'InnoDB');

    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable('user');

    }
}
