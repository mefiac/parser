<?php

namespace Application\Controller;

use Application\Entity\News;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class NewsController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;


    public function __construct($entityManager, $documentManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return void|ViewModel
     */
    public function viewAction()
    {
        $postId = (int)$this->params()->fromRoute('id', 0);
        if ($postId <= 0) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $news = $this->entityManager->getRepository(News::class)
            ->findOneById($postId);

        if ($news == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

       $newsSingle = [
            'fullText' => $news->getFullText(),
            'headerText' => $news->getHeaderText(),
            'link' => $news->getLink(),
            'image'=> $news->getImage(),
        ];

        return new ViewModel([
            'news' => $newsSingle,
        ]);
    }

}
