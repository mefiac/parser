<?php
namespace Application\Controller\Factory;

use Application\Service\NewsManger;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\NewsController;


class NewsControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $newsManager = $container->get(NewsManger::class);

        return new NewsController($entityManager,$newsManager);
    }
}