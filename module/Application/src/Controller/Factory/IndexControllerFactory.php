<?php
namespace Application\Controller\Factory;

use Application\Controller\IndexController;
use Application\Service\LinksManger;
use Application\Service\NewsManger;
use Application\Service\ParserManger;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * This is the factory for IndexController. Its purpose is to instantiate the
 * controller and inject dependencies into it.
 */
class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
         $entityManager  = $container->get('doctrine.entitymanager.orm_default');
         $linksManger    = $container->get(LinksManger::class);
         $parserManager  = $container->get(ParserManger::class);
         $newsManager    = $container->get(NewsManger::class);
        // Instantiate the controller and inject dependencies
        return new IndexController($entityManager, $linksManger, $parserManager, $newsManager);
    }
}