<?php

namespace Application\Controller;

use Application\Service\LinksManger;
use Application\Service\NewsManger;
use Application\Service\ParserManger;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


/**
 * This is the main controller class of the User Demo application. It contains
 * site-wide actions such as Home or About.
 */
class IndexController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var LinksManger
     */
    private $linksManger;

    /**
     * @var ParserManger
     */
    private $parserManager;

    /**
     * @var NewsManger
     */
    private $newsManager;
    /**
     * Constructor. Its purpose is to inject dependencies into the controller.
     */
    public function __construct($entityManager,$linksManger,$parserManager,$newsManager)
    {
        $this->entityManager = $entityManager;
        $this->linksManger = $linksManger;
        $this->parserManager = $parserManager;
        $this->newsManager = $newsManager;
    }

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        /**
         * step 1 parsing
         */
        $linksFromFirstPage = $this->linksManger->getAllLinksFromFirstPage();
        $dataWithFullNews  = $this->parserManager->getAllNews($linksFromFirstPage);
        $this->newsManager->collectAllNews($dataWithFullNews);
        /*
         * step 2 reading
         */
         $newsFromRbk=$this->newsManager->getLastNews();

        return new ViewModel([
            'newsFromRbk' => $newsFromRbk
        ]);
    }


    /**
     * This is the "about" action. It is used to display the "About" page.
     */
    public function aboutAction()
    {
        $appName = 'Zend Framework 3';
        $appDescription = 'Zend Framework 3';

        // Return variables to view script with the help of
        // ViewObject variable container
        return new ViewModel([
            'appName' => $appName,
            'appDescription' => $appDescription
        ]);
    }


}

