<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a registered user.
 * @ORM\Entity(repositoryClass="Application\Repository\NewsRepository")
 * @ORM\Table(name="news")
 */
class News
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="full_text")
     */
    protected $fullText;

    /**
     * @ORM\Column(name="header_text")
     */
    protected $headerText;


    /**
     * @ORM\Column(name="link")
     */
    protected $link;

    /**
     * @ORM\Column(name="image")
     */
    protected $image;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFullText()
    {
        return $this->fullText;
    }

    /**
     * @param mixed $fullText
     */
    public function setFullText($fullText): void
    {
        $this->fullText = $fullText;
    }

    /**
     * @return mixed
     */
    public function getHeaderText()
    {
        return $this->headerText;
    }

    /**
     * @param mixed $headerText
     */
    public function setHeaderText($headerText): void
    {
        $this->headerText = $headerText;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }



}