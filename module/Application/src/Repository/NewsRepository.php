<?php

namespace Application\Repository;

use Application\Entity\News;
use Doctrine\ORM\EntityRepository;

/**
 * This is the custom repository class for Post entity.
 */
class NewsRepository extends EntityRepository
{

    /**
     * @return mixed
     */
    public function getLastNewsFromRepository()
    {
        $entityManager = $this->getEntityManager();
        $lastNews = $entityManager
            ->createQuery('SELECT n.id,SUBSTRING(n.fullText,1,200) as litleText FROM Application\Entity\News n  ORDER BY n.id DESC ')
            ->setMaxResults(15);

        return $lastNews->getResult();
    }

}