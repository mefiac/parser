<?php
namespace Application\Service\Factory;

use Application\Service\NewsManger;
use Interop\Container\ContainerInterface;
use Application\Service\ParserManger;

/**
 * This is the factory class for NavManager service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class NewsMangerFactory
{
    /**
     * This method creates the ParserManger service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new NewsManger($entityManager);
    }
}
