<?php
namespace Application\Service\Factory;

use Interop\Container\ContainerInterface;
use Application\Service\ParserManger;

/**
 * This is the factory class for NavManager service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class ParserMangerFactory
{
    /**
     * This method creates the ParserManger service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        return new ParserManger();
    }
}
