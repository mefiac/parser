<?php
namespace Application\Service\Factory;

use Application\Service\LinksManger;
use Interop\Container\ContainerInterface;


/**
 * This is the factory class for NavManager service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class LinksMangerFactory
{
    /**
     * This method creates the NavManager service and returns its instance. 
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelperManager = $container->get('ViewHelperManager');

        return new LinksManger();
    }
}
