<?php
namespace Application\Service;
use Sunra\PhpSimple\HtmlDomParser;
use Zend\Dom\Query;
use Application\Service\Snoopy;
include('simple_html_dom.php');
/**
 * This service is responsible for determining which items should be in the main menu.
 * The items may be different depending on whether the user is authenticated or not.
 */
class ParserManger
{

    /**
     * Constructs the service.
     */
    public function __construct()
    {

    }

    /**
     * @param $query
     * @return string
     */
    private function getBodyText($linkOfNews)
    {
        $dom = HtmlDomParser::file_get_html( $linkOfNews );

        foreach($dom->find('p') as $e){
            $arr[] = trim($e->innertext);
        }

        return implode($arr);
    }

    /**
     * @param $linkOfNews
     * @return array
     */
    private function readNewsForSingle($linkOfNews)
    {

        $htmlOfPage = file_get_contents($linkOfNews);
        $query = new Query($htmlOfPage);
        $articleOutput['headerOfNews'] =trim($query->execute('.article__header__title')->current()->textContent);
        $articleOutput['articleTextOverview'] =trim($query->execute('.article__text__overview')->current()->textContent);
        $articleOutput['bodyOfNews']   = trim($this->getBodyText($linkOfNews));
        $currentImageCheck=$query->execute('.article__main-image__link img')->current() ?? $query->execute('.article__main-image__link img')->current();
        $articleOutput['imageOfNews']  = ($currentImageCheck) ? $currentImageCheck->getAttribute('src') : null;
        $articleOutput['linkOfNews'] = $linkOfNews;

        return $articleOutput;
    }

    /**
     * @param $linksFromFirstPage
     * @return array
     */
    public function getAllNews($linksFromFirstPage)
    {

        $allFullArticles = [];

        foreach ($linksFromFirstPage as $linkOfNews) {
            array_push($allFullArticles, $this->readNewsForSingle($linkOfNews));
        }

        return $allFullArticles;
    }

}


