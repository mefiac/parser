<?php

namespace Application\Service;

use Zend\Dom\Query;

/**
 * This service is collect lincs from first page
 *
 */
class LinksManger
{

    const FIRST_PAGE = 'https://www.rbc.ru/';

    /**
     * Constructs the service.
     */
    public function __construct()
    {

    }

    /**
     * @return array
     */
    public function getAllLinksFromFirstPage()
    {
        $htmlOfFirstPage = file_get_contents(self::FIRST_PAGE);
        $query = new Query($htmlOfFirstPage);
        $linkForLeadNews = $this->getLinkForLeadNews($query);
        $linkForNews = $this->getLinksForNotLeadNews($query);
        array_push($linkForNews, $linkForLeadNews);

        return $linkForNews;
    }

    /**
     * @param $query
     * @return mixed
     */
    private function getLinkForLeadNews($query)
    {
        /** @var NodeList $results */
        $resultOfSearchingLeadLink = $query->execute('.main__col-main__inner a');

        return $resultOfSearchingLeadLink->current()->getAttribute('href');
    }

    /**
     * @param $query
     * @return array
     */
    private function getLinksForNotLeadNews($query)
    {
        $LinksForNotLeadNews = [];
        $resultOfSearchingNotLeadLinks = $query->execute('.main__col-list a');
        foreach ($resultOfSearchingNotLeadLinks as $resultOfSearchingNotLeadLink) {
            array_push($LinksForNotLeadNews, $resultOfSearchingNotLeadLink->getAttribute('href'));
        }

        return $LinksForNotLeadNews;
    }

}


