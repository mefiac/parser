<?php

namespace Application\Service;

use Application\Entity\News;

/**
 * This service is responsible for determining which items should be in the main menu.
 * The items may be different depending on whether the user is authenticated or not.
 */
class NewsManger
{

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager;
     */
    private $entityManager;

    /**
     * Constructor.
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return mixed
     */
    public function getLastNews()
    {

        return $this->entityManager->getRepository(News::class)->getLastNewsFromRepository();
    }

    /**
     * @param $linkOfNews
     * @return array|void
     */
    public function collectAllNews($dataWithFullNews)
    {
        /**
         * multiple addition as postgres is not supported.
         * The doctrine works with all db
         */
        foreach ($dataWithFullNews as $dataWithOneNews) {

            if($this->entityManager->getRepository(News::class)
                ->findOneByLink($dataWithOneNews['linkOfNews']))  continue;

            /** @var News $news */
            $news = new News();
            $news->setHeaderText($dataWithOneNews['headerOfNews']);
            $news->setFullText($dataWithOneNews['bodyOfNews']);
            $news->setLink($dataWithOneNews['linkOfNews']);
            $news->setImage($dataWithOneNews['imageOfNews']);
            $this->entityManager->persist($news);
            $this->entityManager->flush();
        }


    }


}


